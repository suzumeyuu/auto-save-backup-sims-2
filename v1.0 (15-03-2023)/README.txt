Auto-Save-Backup Sims 2 by SuzumeYuu 

1. About program
2. How to install
3. How to use
4. Custom neighbourhoods

1. About program

Auto-Save-Backup Sims 2 was created to help making backups of neighbourhood in easiest way. The Sims 2 save files includes whole neigbourhood folder (for example N001) and unlike to The Sims 3 you can't create your own save folder and you need to make backups manually. With this program you don't need to remember about making backups anymore!

2. How to install

Download .zip archive from my gitlab (https://gitlab.com/suzumeyuu) and unpack sims2save.bat in Neighbourhoods folder. If you have The Sims 2 Ultimate Collection from Origin (like me), path should look like this: 

C:\Users\User\Documents\EA Games\The Sims™ 2 Ultimate Collection\Neighborhoods

If your disk where you have installed The Sims 2 have different label (D, E, etc.), just replace "C" with your label.

3. How to use

If you want to create backup and run the game, you have to run sims2save.bat first. Next, you will see a black window of cmd (command prompt) with list of neighbourhood you can backup.

REMEMBER! In version 1.0 you can backup ONLY ONE NEIGHBOURHOOD!!!

Then you will be asked to prompt code number of neighbourhood you want to backup (for example N001 - Pleasantview). The case of letters doesn't matter. After commiting your choice with ENTER, program will start to make copy of choosed neighbourhood. 

You can find your backup in C:\Users\User\Documents\EA Games\Auto-Save-Backup Sims 2

If your disk where you have installed The Sims 2 have different label (D, E, etc.), just replace "C" with your label.

Program creates a folder Backup [DD-MM-YYYY] [HH.MM.SS]/[Neighbourhood code]

And finally, program will run Sims2EP9.exe.

4. Custom neighbourhoods

My program have a list with official neighbourhoods created by EA, but you can add on list your neighbourhood too! Even if you don't update list of neighbourhoods, it will work - you only need to know code number of your neighbourhood and input it when program will ask about your neighbourhood code.
