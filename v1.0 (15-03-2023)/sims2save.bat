@echo off

cls

echo Auto-Save-Backup Sims 2 by SuzumeYuu

echo +--------------------------------------------------+
echo	     Which neighbourhood do you wish to backup?    
echo +--------------------------------------------------+			
echo         N001 (Pleasantview)				
echo         N002 (Strangetown)				
echo         N003 (Veronaville)				
echo         E001 (Belladonna Cove)
echo         F001 (Desiderata Valley		
echo         G001 (Riverblossom Hills) 								
echo +--------------------------------------------------+
set /p n="Enter neighbourhood code (example: N001): "

::date
set CUR_YYYY=%date:~6,4%
set CUR_MM=%date:~0,2%
set CUR_DD=%date:~3,2%

::time
set CUR_HH=%time:~0,2%
set CUR_NN=%time:~3,2%
set CUR_SS=%time:~6,2%

set datetime=%CUR_DD%-%CUR_MM%-%CUR_YYYY%_%CUR_HH%.%CUR_NN%.%CUR_SS%

echo Backup in progress, please wait...

xcopy %n% "C:\%homepath%\Documents\EA Games\Auto-Save-Backup Sims 2\Backup %datetime%\%n%" /e /h /c /i

echo Done!

echo Launching the game...

"C:\Program Files (x86)\Origin Games\The Sims 2 Ultimate Collection\Fun with Pets\SP9\TSBin\Sims2EP9.exe"

pause

exit